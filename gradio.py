#!/usr/bin/python
# coding: utf8

import pylast
import sys
import MySQLdb as mariadb
from os import path
import mutagen

####################################   MariaDB INIT  ##############################################
db_connection = mariadb.connect(host='localhost', user='xbmc', passwd='xbmc', db='MyMusic48', use_unicode=True, charset='utf8')
cursor =  db_connection.cursor()

#################################### LASTFM API INIT ##############################################
API_KEY = 'fc1f1da5e21e940c79abc2f1739108cc'
API_SECRET = '269b250a24b2f7903ef435ae425a773a'

username = 'claneys'
password_hash = pylast.md5("56aKhF69")

LFM = pylast.LastFMNetwork(api_key = API_KEY, api_secret = API_SECRET, username = username, password_hash = password_hash)

#################################### INNER NEEDS INIT ##############################################

state_file = '/tmp/ezstream.playlist'
fh_state = open(state_file, 'r')
previous_song = fh_state.readlines()
fh_state.close()
fh_state = open(state_file, 'a')
log_file = '/tmp/ezstream.log'
fh_log = open(log_file, 'a')

###################################### Main ########################################################

def format_request():
    fetch = cursor.fetchone()
    request_result = u''.join(fetch)
    request_result = request_result.replace(u'smb://GILGAMESH/Share', u'/data')
    fh_state.write('%s\n' % request_result.encode('utf8'))
    
    return request_result

def get_random_song():
    cursor.execute(u'SELECT strPath,strFileName FROM path, song WHERE song.idPath=path.idPath ORDER BY RAND() LIMIT 1;')
    random_song = format_request()
    
    return random_song

def get_metadata():
    cfile = previous_song[-1].rstrip()
    try:
        metadata = mutagen.File(cfile, easy=True)
    except:
        album = path.basename(path.dirname(cfile))
        artist = path.basename(path.dirname(path.dirname(cfile)))
        title = path.basename(cfile).rsplit('.')[0]
        metadata = { 'artist' : (artist,), 'title' : (title,) }

    return metadata

def already_played(song_to_play):
    fh = open(state_file, 'r')
    songs_played = [line.rstrip() for line in fh.readlines()]
    fh.close()
    if song_to_play.encode('utf8') in songs_played:
        fh_log.write('Already played, next !\n')
        return True
    return False

def get_next_song():
    if previous_song != []:
        fh_log.write('Previous song is %s.\n' % previous_song[-1].rstrip())
        song = None
        # Get similar song into our media library if not exact match then get one random song from artist
        # else get a totaly random song
        try:
            last_track = LFM.get_track(get_metadata()['artist'][0], get_metadata()['title'][0])
            last_artist = get_metadata()['artist'][0]
        except KeyError:
            last_track = LFM.get_track(get_metadata()['Author'][0].value, get_metadata()['Title'][0].value)
            last_artist = get_metadata()['Author'][0].value

        try:
            for similar in last_track.get_similar():
                artist = similar[0].artist.get_name().replace('\'', '')
                title = similar[0].title.replace('\'', '')

                if artist == last_artist:
                    continue

                if cursor.execute(u"SELECT strPath, strFileName FROM path, song WHERE song.idPath=path.idPath AND song.strArtists LIKE \'%s\' AND song.strTitle LIKE \'%s\' LIMIT 1;" % (artist, title)) > 0:
                    song = format_request()
                    fh_log.write('Get a perfect match. From %s to %s - %s\n' % (last_track, artist.encode('utf8'), title.encode('utf8')))
                elif cursor.execute(u"SELECT strPath, strFileName FROM path, song WHERE song.idPath=path.idPath AND song.strArtists LIKE \'%s\' ORDER BY RAND() LIMIT 1;" % artist) > 0:
                    song = format_request()
                    fh_log.write('Get a match. From %s to %s\n' % (last_track, artist.encode('utf8')))

                # Check that we did not already play that song
                if song:
                    if already_played(song):
                        continue
                    else:
                        print(song.encode('utf8'))
                        sys.exit(0)
        except pylast.WSError:
            fh_log.write('Track not found... ')
            pass

        # Damned we did not get any good song, go random 
        print(get_random_song().encode('utf8'))
        fh_log.write('Can\'t get a match get a random song, so...\n')
        sys.exit(0)
    
    print(get_random_song().encode('utf8'))
    fh_log.write('Radio start. Getting a random song and run, run, run.\n')

get_next_song()
